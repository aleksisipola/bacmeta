These two .zip-files both contain the input and output files of Bacmeta run.

FeatureExample displays small demonstration run displaying all but one of output files of Bacmeta, with all simulation features toggled on.

CaseExample displays use case where 3 connected populations are simulated for 20 000 generations in specific migration setting and resulting pairwise distances are documented. 


Both run settings can be reused either by using the provided .input-files, or using the input documentation files rundetails*.txt and migrationinput*.txt by copying and renaming them to input files.



